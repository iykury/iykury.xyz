var old, oldProb, oldMax;

function update(){
	var input = document.getElementById("regular").value;
	var prob = document.getElementById("prob").value;
	var max = document.getElementById("max").value;
	var text = "";

	if(input != old || prob != oldProb || max != oldMax){
		for(var id = 0; id < input.length; id++){
			var chr = input[id];
			
			//Make compatible with characters above U+FFFF
			//Not strictly necessary but this code was already here from the shavian to ipa converter so iy miyt as well use it
			codePoint = chr.charCodeAt(0);
			if(codePoint > 0xd800 && codePoint < 0xe000){
				nextChr = input[id + 1];
				nextCodePoint = nextChr.charCodeAt(0);
				if(nextCodePoint > 0xd800 && nextCodePoint < 0xe000){
					chr = chr + nextChr;
					id++;
				}
			}

			for(var i = 0; i < max; i++){
				if(Math.random() < prob)
					chr += String.fromCodePoint(Math.floor(0x0300 + 0x5c * Math.random()));
			}

			text += chr;
		}
		document.getElementById("zalgo").value = text;
	}

	old = document.getElementById("regular").value;
	oldProb = document.getElementById("prob").value;
	oldMax = document.getElementById("max").value;
}

loop = window.setInterval(update, 50);
