var old;

function update(){
    var old;
    var input = s("#latin").value;
    var latin = false;
    var text = "";

    if(input != old){
        for(id in input){
            char = input[id];
            if(latin){
                if(char == "`"){
                    char = "";
                    latin = false;
                }
            }else{
                switch(char){
                    case "a":
                        char = "𐑨";
                        break;
                    case "b":
                        char = "𐑚";
                        break;
                    case "c":
                        char = "𐑾";
                        break;
                    case "d":
                        char = "𐑛";
                        break;
                    case "e":
                        char = "𐑧";
                        break;
                    case "f":
                        char = "𐑷";
                        break;
                    case "g":
                        char = "𐑜";
                        break;
                    case "h":
                        char = "𐑣";
                        break;
                    case "i":
                        char = "𐑦";
                        break;
                    case "j":
                        char = "𐑘";
                        break;
                    case "k":
                        char = "𐑒";
                        break;
                    case "l":
                        char = "𐑤";
                        break;
                    case "m":
                        char = "𐑥";
                        break;
                    case "n":
                        char = "𐑯";
                        break;
                    case "o":
                        char = "𐑪";
                        break;
                    case "p":
                        char = "𐑐";
                        break;
                    case "q":
                        char = "𐑙";
                        break;
                    case "r":
                        char = "𐑮";
                        break;
                    case "s":
                        char = "𐑕";
                        break;
                    case "t":
                        char = "𐑑";
                        break;
                    case "u":
                        char = "𐑫";
                        break;
                    case "v":
                        char = "𐑭";
                        break;
                    case "w":
                        char = "𐑢";
                        break;
                    case "x":
                        char = "·";
                        break;
                    case "y":
                        char = "𐑩";
                        break;
                    case "z":
                        char = "𐑟";
                        break;
                    case "A":
                        char = "𐑲";
                        break;
                    case "B":
                        char = "𐑝";
                        break;
                    case "C":
                        char = "𐑽";
                        break;
                    case "D":
                        char = "𐑞";
                        break;
                    case "E":
                        char = "𐑱";
                        break;
                    case "F":
                        char = "𐑶";
                        break;
                    case "G":
                        char = "𐑡";
                        break;
                    case "H":
                        char = "𐑣";
                        break;
                    case "I":
                        char = "𐑰";
                        break;
                    case "J":
                        char = "𐑿";
                        break;
                    case "K":
                        char = "𐑗";
                        break;
                    case "L":
                        char = "𐑻";
                        break;
                    case "M":
                        char = "𐑸";
                        break;
                    case "N":
                        char = "𐑹";
                        break;
                    case "O":
                        char = "𐑴";
                        break;
                    case "P":
                        char = "𐑓";
                        break;
                    case "Q":
                        char = "𐑙";
                        break;
                    case "R":
                        char = "𐑼";
                        break;
                    case "S":
                        char = "𐑖";
                        break;
                    case "T":
                        char = "𐑔";
                        break;
                    case "U":
                        char = "𐑵";
                        break;
                    case "V":
                        char = "𐑬";
                        break;
                    case "W":
                        char = "𐑺";
                        break;
                    case "X":
                        char = "·";
                        break;
                    case "Y":
                        char = "𐑳";
                        break;
                    case "Z":
                        char = "𐑠";
                        break;
                    case "`":
                        char = "";
                        latin = true;
                        break;
                }
            }
            text += char;
        }
        s("#shavian").value = text;
    }

    old = s("#latin").value;
}

loop = window.setInterval(update, 50);