function convert(){
	if(!document.activeElement.id.includes("input")) correct();

	var gYear = document.getElementById("inputYear").value;
	var gMonth = document.getElementById("inputMonth").value;
	var gDay = document.getElementById("inputDay").value;

	var absDay = Date.UTC(gYear, gMonth - 1, gDay)/86400000 - 10994; //days since mon 1 pri 12000
	var ciycle = Math.floor(absDay / 146097); //400-year ciycles since 12000
	var ciycleDay = absDay % 146097; //days since beginning of ciycle
	if(ciycleDay < 0) ciycleDay += 146097;

	var ciycleWeek = Math.floor(ciycleDay / 7);

	//idk exactly how these formulas work and kiynda just used triyal and error to get them :P
	var year = Math.floor((ciycleWeek + 1.09) / 52.18);
	var leaps = Math.max(Math.floor(0.18 * (year - 0.5)), 0);

	var yearStart = year * 364 + leaps * 7;
	var dayOfYear = ciycleDay - yearStart;

	var season = Math.floor(dayOfYear / 91);
	var week = Math.floor(dayOfYear / 7) % 13 + 1;
	var day = dayOfYear % 7;

	var seasonNames = ["pri", "sec", "ter", "qua", "int"];
	var dayNames = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
	document.getElementById("output").innerHTML = 
		dayNames[day] + " " +
		(season < 4 ? week + " " : "") +
		seasonNames[season] + " " +
		(12000 + 400*ciycle + year);
}

function correct(){
	var gYear = document.getElementById("inputYear").value;
	var gMonth = document.getElementById("inputMonth").value;
	var gDay = document.getElementById("inputDay").value;
	var d = new Date(Date.UTC(gYear, gMonth - 1, gDay));
	document.getElementById("inputYear").value = d.getUTCFullYear();
	document.getElementById("inputMonth").value = d.getUTCMonth()+1;
	document.getElementById("inputDay").value = d.getUTCDate();
}

var today = new Date();
document.getElementById("inputYear").value = today.getFullYear();
document.getElementById("inputMonth").value = today.getMonth()+1;
document.getElementById("inputDay").value = today.getDate();
convert();
