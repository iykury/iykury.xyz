var initials = ["b", "ch", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "sh", "t", "th", "v", "w", "y", "z"];
var middles = ["bb", "tch", "dd", "ff", "gg", "dge", "ck", "ll", "mm", "nn", "pp", "rr", "ss", "sh", "tt", "th", "v", "z"];
var finals = ["b", "tch", "d", "f", "g", "dge", "ck", "ll", "m", "n", "p", "r", "ss", "sh", "t", "th", "ve", "z"];
var vowels = ["a", "e", "i", "o", "u"];

function generate(){
    s("#words"). innerHTML = "";
    for(var i = 0; i < s("#number").value; i++){
        //Generate random letters
        var initial = initials[Math.floor(Math.random() * initials.length)];
        var vowel1 = vowels[Math.floor(Math.random() * vowels.length)];
        var middle = middles[Math.floor(Math.random() * middles.length)];
        var vowel2 = vowels[Math.floor(Math.random() * vowels.length)];
        var final = finals[Math.floor(Math.random() * finals.length)];

        //Clean up specific consonant-vowel pairs so they look better
        if(middle == "dge" && (vowel2 == "e" || vowel2 == "i")){ //dgee, dgei -> dge, dgi
            middle = "dg";
        }
        if(middle == "dge" && vowel2 == "u"){ //dgeu -> ju
            middle = "j";
        }
        if(initial == "k" && !(vowel1 == "e" || vowel1 == "i")){ //ka, ko, ku -> ca, co, cu
            initial = "c";
        }

        //Put all the parts together
        s("#words").innerHTML += `${initial}${vowel1}${middle}${vowel2}${final}<br>`;
    }
}

