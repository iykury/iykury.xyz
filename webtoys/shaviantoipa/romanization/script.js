var old;

function update(){
	var old;
	var input = s("#shavian").value;
	var text = "";

	if(input != old){
		for(var id = 0; id < input.length; id++){
			char = input[id];

			//JS uses UTF-16 internally so I have to do this shit to make it work
			codePoint = char.charCodeAt(0);
			if(codePoint > 0xd800 && codePoint < 0xe000){
				nextChar = input[id + 1];
				nextCodePoint = nextChar.charCodeAt(0);
				if(nextCodePoint > 0xd800 && nextCodePoint < 0xe000){
					char = char + nextChar;
					id++;
				}
			}

			switch(char){
				case "𐑐":
					char = "p";
					break;
				case "𐑑":
					char = "t";
					break;
				case "𐑒":
					char = "k";
					break;
				case "𐑓":
					char = "f";
					break;
				case "𐑔":
					char = "þ";
					break;
				case "𐑕":
					char = "s";
					break;
				case "𐑖":
					char = "ś";
					break;
				case "𐑗":
					char = "c";
					break;
				case "𐑘":
					char = "y";
					break;
				case "𐑙":
					char = "ŋ";
					break;
				case "𐑚":
					char = "b";
					break;
				case "𐑛":
					char = "d";
					break;
				case "𐑜":
					char = "g";
					break;
				case "𐑝":
					char = "v";
					break;
				case "𐑞":
					char = "ð";
					break;
				case "𐑟":
					char = "z";
					break;
				case "𐑠":
					char = "ź";
					break;
				case "𐑡":
					char = "j";
					break;
				case "𐑢":
					char = "w";
					break;
				case "𐑣":
					char = "h";
					break;
				case "𐑤":
					char = "l";
					break;
				case "𐑥":
					char = "m";
					break;
				case "𐑦":
					char = "i";
					break;
				case "𐑧":
					char = "e";
					break;
				case "𐑨":
					char = "a";
					break;
				case "𐑩":
					char = "ə";
					break;
				case "𐑪":
					char = "o";
					break;
				case "𐑫":
					char = "û";
					break;
				case "𐑬":
					char = "au";
					break;
				case "𐑭":
					char = "ā";
					break;
				case "𐑮":
					char = "r";
					break;
				case "𐑯":
					char = "n";
					break;
				case "𐑰":
					char = "ī";
					break;
				case "𐑱":
					char = "ē";
					break;
				case "𐑲":
					char = "ai";
					break;
				case "𐑳":
					char = "u";
					break;
				case "𐑴":
					char = "ō";
					break;
				case "𐑵":
					char = "ū";
					break;
				case "𐑶":
					char = "oi";
					break;
				case "𐑷":
					char = "ô";
					break;
				case "𐑸":
					char = "ār";
					break;
				case "𐑹":
					char = "ôr";
					break;
				case "𐑺":
					char = "êr";
					break;
				case "𐑻":
					char = "ûr";
					break;
				case "𐑼":
					char = "ər";
					break;
				case "𐑽":
					char = "iər";
					break;
				case "𐑾":
					char = "iə";
					break;
				case "𐑿":
					char = "yū";
					break;
			}
			text += char;
		}
		s("#romanization").value = text;
	}

	old = s("#shavian").value;
}

loop = window.setInterval(update, 50);
