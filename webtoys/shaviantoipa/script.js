var old;

function update(){
	var old;
	var input = s("#shavian").value;
	var text = "";

	if(input != old){
		for(var id = 0; id < input.length; id++){
			char = input[id];

			//JS uses UTF-16 internally so I have to do this shit to make it work
			codePoint = char.charCodeAt(0);
			if(codePoint > 0xd800 && codePoint < 0xe000){
				nextChar = input[id + 1];
				nextCodePoint = nextChar.charCodeAt(0);
				if(nextCodePoint > 0xd800 && nextCodePoint < 0xe000){
					char = char + nextChar;
					id++;
				}
			}

			switch(char){
				case "𐑐":
					char = "p";
					break;
				case "𐑑":
					char = "t";
					break;
				case "𐑒":
					char = "k";
					break;
				case "𐑓":
					char = "f";
					break;
				case "𐑔":
					char = "θ";
					break;
				case "𐑕":
					char = "s";
					break;
				case "𐑖":
					char = "ʃ";
					break;
				case "𐑗":
					char = "t͡ʃ";
					break;
				case "𐑘":
					char = "j";
					break;
				case "𐑙":
					char = "ŋ";
					break;
				case "𐑚":
					char = "b";
					break;
				case "𐑛":
					char = "d";
					break;
				case "𐑜":
					char = "ɡ";
					break;
				case "𐑝":
					char = "v";
					break;
				case "𐑞":
					char = "ð";
					break;
				case "𐑟":
					char = "z";
					break;
				case "𐑠":
					char = "ʒ";
					break;
				case "𐑡":
					char = "d͡ʒ";
					break;
				case "𐑢":
					char = "w";
					break;
				case "𐑣":
					char = "h";
					break;
				case "𐑤":
					char = "l";
					break;
				case "𐑥":
					char = "m";
					break;
				case "𐑦":
					char = "ɪ";
					break;
				case "𐑧":
					char = "ɛ";
					break;
				case "𐑨":
					char = "æ";
					break;
				case "𐑩":
					char = "ə";
					break;
				case "𐑪":
					char = "ɒ";
					break;
				case "𐑫":
					char = "ʊ";
					break;
				case "𐑬":
					char = "aʊ̯";
					break;
				case "𐑭":
					char = "ɑː";
					break;
				case "𐑮":
					char = "r";
					break;
				case "𐑯":
					char = "n";
					break;
				case "𐑰":
					char = "iː";
					break;
				case "𐑱":
					char = "eɪ̯";
					break;
				case "𐑲":
					char = "aɪ̯";
					break;
				case "𐑳":
					char = "ʌ";
					break;
				case "𐑴":
					char = "oʊ̯";
					break;
				case "𐑵":
					char = "uː";
					break;
				case "𐑶":
					char = "ɔɪ̯";
					break;
				case "𐑷":
					char = "ɔː";
					break;
				case "𐑸":
					char = "ɑːr";
					break;
				case "𐑹":
					char = "ɔːr";
					break;
				case "𐑺":
					char = "ɛə̯r";
					break;
				case "𐑻":
					char = "ɜːr";
					break;
				case "𐑼":
					char = "ər";
					break;
				case "𐑽":
					char = "ɪə̯r";
					break;
				case "𐑾":
					char = "iə";
					break;
				case "𐑿":
					char = "juː";
					break;
			}
			text += char;
		}
		s("#ipa").value = text;
	}

	old = s("#shavian").value;
}

loop = window.setInterval(update, 50);
