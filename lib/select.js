function chrsToStr(chrs){
    string = "";
    for(c = 0; c < chrs.length; c++){
        string += chrs[c];    
    }
    return string;
}

function s(arg){
    arg = arg.split("");
    if(arg[0] == "#"){
        arg.shift();
        arg = chrsToStr(arg);
        return document.getElementById(arg);
    }else if(arg[0] == "."){
        arg.shift();
        arg = chrsToStr(arg);
        return document.getElementsByClassName(arg);
    }
}
